from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_level_with_fit
import datetime


def test_plot_water_levels():
    station_to_test = MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1")
    levels = [0, 1, 2, 3]
    dates = [datetime.datetime(2019, 12, 1), datetime.datetime(2019, 12, 2), 
            datetime.datetime(2019, 12, 3), datetime.datetime(2019, 12, 4)]

    plot_water_levels(station_to_test, dates, levels)


def test_plot_water_level_with_fit():
    station_to_test = MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1")
    levels = [0, 1, 2, 3]
    dates = [datetime.datetime(2019, 12, 1), datetime.datetime(2019, 12, 2), 
            datetime.datetime(2019, 12, 3), datetime.datetime(2019, 12, 4)]
    
    plot_water_level_with_fit(station_to_test, dates, levels, 3)

