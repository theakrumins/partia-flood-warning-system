from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_station_data
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river


MonitoringStations = build_station_list()
RiversWithStations = rivers_with_station(MonitoringStations)

#print(MonitoringStations)
#print(RiversWithStations)

RiversWithStations = list(RiversWithStations)
RiversWithStations = sorted(RiversWithStations)
for i in range(0, 10):
    print(RiversWithStations[i])
    


StationsOnRiver = stations_by_river(MonitoringStations)
StationsOnRiver = sorted(StationsOnRiver["River Aire"])
print(StationsOnRiver)
#print(MonitoringStations[0].name)
