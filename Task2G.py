from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
from floodsystem.analysis import gradient_positive
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels


'''stations = build_station_list()
stations_over_rel_tol = stations_over_threshold(stations, 2)
stations_against_water_levels = []

for station in stations_over_rel_tol:
    if station.latest_level:
        stations_against_water_levels.append([station, station.measure_id, station.latest_level])
    else:
        pass'''

def risk_level(stations, p, dt=2):
    for station in stations:
        if station.relative_water_level():
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
            
            if len(dates) > 0:
                gradient_is_positive = gradient_positive(dates, levels, p)
                relative_level = station.relative_water_level()
                
                if gradient_is_positive and relative_level > 0.8:
                    print("Risk level : Low at : {}".format(station.name))
                    
                elif not gradient_is_positive and relative_level > 1.0:
                    print("Risk level : Moderate at : {}".format(station.name))
                    
                elif gradient_is_positive and relative_level > 1.0:
                    print("Risk level : High at : {}".format(station.name))
                    
                elif not gradient_is_positive and relative_level > 2.0:
                    print("Risk level : High at : {}".format(station.name))
                    
                elif gradient_is_positive and relative_level > 2.0:
                    print("Risk level : Severe at : {}".format(station.name))
                
                else:
                    print("Risk level: None at : {}".format(station.name))
                                    
                           


stations = build_station_list()
stations_to_test = [stations[0], stations[1], stations[2], stations[3], stations[4]]

update_water_levels(stations)

risk_level(stations_to_test, 4)    