import numpy as np
import matplotlib.pyplot as plt
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime
from floodsystem.utils import sorted_by_key
from floodsystem.analysis import polyfit

station_list = build_station_list()
update_water_levels(station_list)
stations_against_water_levels = []

for station in station_list:
    if station.latest_level:
        stations_against_water_levels.append([station, station.measure_id, station.latest_level])
    else:
        pass

stations_against_water_levels = sorted_by_key(stations_against_water_levels, 2, True)

for i in range(0, 5):
    "take top 5 stations, grab their levels and dates and plot them"
    dates, levels = fetch_measure_levels(stations_against_water_levels[i][0].measure_id, dt=datetime.timedelta(2))
    plot_water_level_with_fit(stations_against_water_levels[i][0], dates, levels, 3)
    
    