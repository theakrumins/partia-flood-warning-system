from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_station_data
from floodsystem.geo import stations_within_radius

MonitoringStations = build_station_list()
cambridge = (52.2053, 0.1218)
list_within_distance = stations_within_radius(MonitoringStations,cambridge, 10)
print(list_within_distance)
