from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.station import MonitoringStation

monitoring_stations = build_station_list()
inconsistent_stations = inconsistent_typical_range_stations(monitoring_stations)
print(inconsistent_stations)

