import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from floodsystem.analysis import polyfit
import datetime

def test_polyfit():
    x = [datetime.datetime(2019, 12, 1), datetime.datetime(2019, 12, 2), 
        datetime.datetime(2019, 12, 3), datetime.datetime(2019, 12, 4)]
    y = np.linspace(0, 30, 30)

    poly = polyfit(x, y, 3)

    assert poly[1] == [ 1.59666696e-15, -7.34260218e-15,  1.00000000e+01,  2.65039321e-15] and poly[1] == [737394.0]


'''import numpy as np
from floodsystem.analysis import polyfit
import datetime


x = [datetime.datetime(2019, 12, 1), datetime.datetime(2019, 12, 2), 
    datetime.datetime(2019, 12, 3), datetime.datetime(2019, 12, 4)]
y = np.linspace(0, 30, 4)

z = polyfit(x, y, 3)
print(z)'''

x = [datetime.datetime(2019, 12, 1), datetime.datetime(2019, 12, 2), 
    datetime.datetime(2019, 12, 3), datetime.datetime(2019, 12, 4)]
y = np.linspace(0, 30, 4)

poly = polyfit(x, y, 3)
ply = poly[0].tolist()
shift = poly[1]
print(ply)

assert ply == [1.5966669565974488e-15, -7.342602177706384e-15, 10.00000000000001, 2.650393212810174e-15] and shift == 737394.0
