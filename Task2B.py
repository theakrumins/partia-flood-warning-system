from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.datafetcher import fetch_station_data
from floodsystem.flood import stations_over_threshold

MonitoringStations = build_station_list()
high_level_stations = stations_over_threshold(MonitoringStations, 0.8)
print(high_level_stations)