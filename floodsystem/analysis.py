import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def polyfit(dates, levels, p):
    "take the dates and levels from a station and fit a poly"
    "order p to the data, using least squares method"
    num_dates = matplotlib.dates.date2num(dates)
    poly = np.polyfit(num_dates - num_dates[0], levels, 3)
    d0 = num_dates[0]
    return (poly, d0)


def gradient_positive(dates, levels, p):
    "return true if the gradient is positive"
    num_dates = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(num_dates - num_dates[0], levels, p)[0]
    poly = np.poly1d(p_coeff)

    if (poly(num_dates[-2]) - poly(num_dates[-1])) > 0:
        return False
    else:
        return True
