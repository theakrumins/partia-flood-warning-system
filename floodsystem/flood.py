from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_station_data
from .utils import sorted_by_key

def stations_over_threshold(stations, tol):
    results = []
    for station in stations:
        if station.relative_water_level() != None and station.relative_water_level() > tol:
            data = tuple(station.name, station.relative_water_level())
            results.append(data)
    return results


def stations_highest_rel_level(stations, N):
    for station in stations:
        data = []
        if station.relative_water_level() != None:
            entry = tuple(station.name, station.relative_water_level())
            data.append(entry)
    sorted_by_key(data, 1, reverse = False)
    counter = 0
    results = []
    while counter < N:
        results.append(data[counter])
        counter += 1
    return results
    