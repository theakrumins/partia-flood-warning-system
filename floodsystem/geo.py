# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa

def rivers_with_station(stations):
    #this returns a set containing all the rivers which have a montoring station on them
    rivers = set([])
    for station in stations:
        rivers.add(station.river)
        #print(station.river)
    return rivers

def stations_by_river(stations):
    #this returns a dictionary with the keys being rivers and the stations on that river being the values
    rivers_and_stations = {}
            
    for i in range(0,len(stations)-1):
        #"take each river from the rivers list" 
        #"and return the river as the key and the stations" 
        #"which it is attached to as the values"
               
        if stations[i].river in rivers_and_stations:
            rivers_and_stations[stations[i].river] += [stations[i].name]
            rivers_and_stations[stations[i].river].sort
        else:
            rivers_and_stations[stations[i].river] = [stations[i].name]
    rivers_and_stations = rivers_and_stations
    return rivers_and_stations



def rivers_by_station_number(stations, N):
#this returns the N rivers with the most 
    num_of_stations = {}

    for i in range(0, len(stations)-1):
        
        if stations[i].river in num_of_stations:
            num_of_stations[stations[i].river] +=1
        else: 
            num_of_stations[stations[i].river] = 1

    top_n = num_of_stations.items()
    top_n = sorted_by_key(top_n, 1, True)
    
    names = []
    for i in range(0, len(top_n)):
        if top_n[i][1] >= top_n[N-1][1]:
            names += top_n[i]

    return names

def stations_by_distance(stations, p):
    '''this returns a list of tuples contating station location and distance tuples which are sorted by distance from a give nset of coordinates'''
    from haversine import haversine
    if len(p) != 2 or not isinstance(p, tuple):
        print("position has not been given as a tuple of latitude and longitude coordinates")
    data = []
    for station in stations:
        coordinate = tuple(station.coord)
        distance = float(haversine(p, coordinate))
        name = str(station.name) + ", " + str(station.town)
        info = (name, distance)
        data.append(tuple(info))
    data=sorted_by_key(data, 1)
    return data

def stations_within_radius(stations, centre, r):

    from haversine import haversine
    if len(centre) != 2 or not isinstance(centre, tuple):
        print("position has not been given as a tuple of latitude and longitude coordinates")
    data = []
    for station in stations:
        coordinate = tuple(station.coord)
        distance = float(haversine(centre, coordinate))
        info = station.name
        if distance < r:
            data.append(info)
    data.sort()
    return data
