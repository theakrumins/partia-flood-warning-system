import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib
import datetime
import numpy as np
from .analysis import polyfit

def plot_water_levels(station, dates, levels):
    import matplotlib.pyplot as plt
    from datetime import datetime, timedelta
 
    plt.plot(dates, levels)
    plt.xlabel('date')
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title("Station :" + station.name)

    plt.tight_layout()
    plt.show()
    


def plot_water_level_with_fit(station, dates, levels, p):
    #get polynomial coefficients and shift and separate
    fit = polyfit(dates, levels, 3)
    p_coeff = fit[0]
    shift = fit[1]

    #turn coeff into something evaluable
    poly = np.poly1d(p_coeff)

    #turn dates into numbers and evaluate their polynomial
    x = matplotlib.dates.date2num(dates)
    y = levels
    plt.plot(x - shift, y, '.')
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1 - shift, poly(x1 - x[0]))
    plt.xlabel('Time (days ago)')
    plt.ylabel('Water Level (m)')
    plt.title("Station : " + station.name)
    plt.tight_layout()
    plt.show()