from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

MonitoringStations = build_station_list()

top_N_rivers = rivers_by_station_number(MonitoringStations, 10)
print(top_N_rivers)
