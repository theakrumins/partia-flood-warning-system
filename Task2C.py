from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.datafetcher import fetch_station_data
from floodsystem.flood import stations_highest_rel_level

MonitoringStations = build_station_list()
worrying_stations = stations_highest_rel_level(MonitoringStations, 10)

for entry in worrying_stations:
    print(entry)

