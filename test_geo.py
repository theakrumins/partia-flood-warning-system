import floodsystem.geo as geo
from floodsystem.station import MonitoringStation
from haversine import haversine

def test_rivers_by_station_number():
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_2", "measure_id_2", "label_2", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_3", "measure_id_3", "label_3", (0.0, 0.0), (0.0, 1.0), "river_2", "town_1"),
                MonitoringStation("id_4", "measure_id_4", "label_4", (0.0, 0.0), (0.0, 1.0), "river_3", "town_1"),
                MonitoringStation("id_5", "measure_id_5", "label_5", (0.0, 0.0), (0.0, 1.0), "river_3", "town_1")]
    
    rivers = geo.rivers_by_station_number(stations, 1)
    assert(rivers == [("river_1", 2), ("river_3", 2)])


def test_stations_by_distance():
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_2", "measure_id_2", "label_2", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1")]
    final_result = geo.stations_by_distance(stations, (0,0))
    assert final_result == [(stations[0], 0), (stations[1], haversine((0, 0), (1, 0)))]


def test_stations_within_radius():
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (-1.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_2", "measure_id_2", "label_2", (1.0, 0.0), (15.0, 17.0), "river_1", "town_1")]
    final_result = geo.stations_within_radius(stations, (1.0, 0.0), 3)
    assert final_result == ([stations[0]])


def test_rivers_with_stations():
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_2", "measure_id_2", "label_2", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_3", "measure_id_3", "label_3", (0.0, 0.0), (0.0, 1.0), "river_2", "town_1")]
    list_of_rivers = geo.rivers_with_station(stations)
    assert list_of_rivers == {"river_1", "river_2"}


def test_stations_by_river():
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_2", "measure_id_2", "label_2", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_3", "measure_id_3", "label_3", (0.0, 0.0), (0.0, 1.0), "river_2", "town_1"),
                MonitoringStation("id_4", "measure_id_4", "label_4", (0.0, 0.0), (0.0, 1.0), "river_3", "town_1")]
    dictionary_of_rivers = geo.stations_by_river(stations)
    assert dictionary_of_rivers == {"river_1": ["label_1", "label_2"], "river_2":["label_3", "label_4"]}