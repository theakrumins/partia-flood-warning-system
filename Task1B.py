from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_station_data
from floodsystem.geo import stations_by_distance


MonitoringStations = build_station_list()
cambridge = (52.2053, 0.1218)
distance_list = stations_by_distance(MonitoringStations, cambridge)
for entry in distance_list[:-5]:
    print(entry)